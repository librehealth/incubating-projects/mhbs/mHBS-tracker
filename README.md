

Check the [Wiki](https://github.com/dhis2/dhis2-android-capture-app/wiki) for information about how to build the project and its architecture **(WIP)**

### What is this repository for? ###

MHBS Tracker Android application.

### Importing DHIS2 Metadata ###

To configure the DHIS2 to use this app, the below two Metadata files need to be imported to create programs, data elements etc.

* [HBB Program Metadata](metadata-hbb-program.json)
* [HBB Survey Metadata](metadata-hbb-survey.json)

### QR code for mHBS app ###
![bmgfdev-qr.png](/docs/bmgfdev-qr.png)
